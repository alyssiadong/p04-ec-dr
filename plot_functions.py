# -*- coding: utf-8 -*-
"""
Created on Thu Aug 22 14:10:09 2019

@author: pccash
"""
import csv
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.dates as mdates
from mpl_toolkits.mplot3d import Axes3D
import matplotlib
import itertools
from collections import namedtuple
import datetime
import pandas as pd
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
# plt.figure(num=None, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')


def cost_fun(p_mis, u, loss):
    c = (p_mis - u) ** 2 + loss**2
    return c

def vector_read(filename, n, header=0):
    x = np.loadtxt(filename)
    x = x[header:n]
    return x

def write_values(x, f):
    a = open(f, "w+")
    np.savetxt(f, x, fmt="%s")


def vector_init(n):
    vector = []
    for i in range(n):
        vector.append(0)
    return vector

def inter_policy(x_m, z_inter, x_int_v):
    for i in range(len(x_int_v)):
        z_inter = inter_1d(x_m[i], z_inter, x_int_v[i])
    return z_inter

def inter_1d(x, z, x_int):

    i_x_next = 0
    #round(x_int, 5)
    #if x_int > max(x):
    #    print('attention : interpolation value is higher than maximum state vector boundries')
    #    print(x_int, 'x_int')
    #    x_int = max(x)

    while i_x_next <= len(x) - 1 and x[i_x_next] <= x_int:
        i_x_next = i_x_next + 1

    if x_int < min(x):
        dx = x[1] - x[0]
        z_int = z[0] + (x_int-x[0])/dx * (z[1]-z[0])

    elif x_int > max(x) :
        dx = x[len(x)-1] - x[len(x)-2]
        z_int = z[len(x)-1] + (x_int-x[len(x)-1])/dx * (z[len(x)-1]-z[len(x)-2])

    elif x_int == min(x):
        z_int = z[0]

    elif x_int == max(x):
        z_int = z[len(x)-1]

    else:
        dx = x[i_x_next]-x[i_x_next-1]
        z_int = z[i_x_next - 1] + (x_int - x[i_x_next - 1]) / dx * (z[i_x_next] - z[i_x_next - 1])

    #if z_int < 0:
    #    print('warning : cost is negative', z_int)
    #print('z_int', z_int, 'x_int', x_int)
    if np.isnan(z_int.any()) :
        print('Naaaaaaaaaaaaaaaaaaaaaaan')
    return z_int

def plot_3d(x_min, x_max, n_x, y_min, y_max, n_y, z, title='', x_label='', y_label='', rcount=50, ccount=50):
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Make data.
    dx = (x_max - x_min) / (n_x-1)
    dy = (y_max - y_min) / (n_y-1)
    x = np.arange(x_min, x_max+dx, dx)
    y = np.arange(y_min, y_max+dy, dy)
    X, Y = np.meshgrid(x, y)
    # Plot the surface.
    surf = ax.plot_surface(X, Y, z, cmap=cm.coolwarm, rcount=rcount, ccount=ccount)

    # Customize the z axis.
    ax.set_zlim(np.amin(z), np.amax(z))
    ax.zaxis.set_major_locator(LinearLocator(10))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

    # Add a color bar which maps values to colors.
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.title(title)
    plt.ylabel(y_label)
    plt.xlabel(x_label)

    plt.show()
    return

def plot_1d_comerror(df, df_mon):
    num = 2
    T = ['$P_{controller, NFTL}$', '$P_{monitor, NFTL}$','$P_{NFL}$','$P_{controller,EV}$','$P_{controller, TES}$','$P_{TES}$']
    l = [None] * 4
    ll = [None] *2

    # x_lim_min=n.values
    # x_lim_min=x_lim_min[0]
    # x_lim_max=n.values
    # x_lim_max =x_lim_max[-1]

    fig, (ax1,ax2) = plt.subplots(num,1,sharex=True)

    # t = t.data.to_pydatetime()

    #ax = plt.gca()

    font = {'family': 'serif', 'serif': 'Computer Modern Roman',
            'size': 16}
    #t=pd.to_datetime(n).dt.time
    #t=n

    a= '#33a02c'
    b= '#1f78b4'
    c= '#e31a1c'
    d='#6a3d9a'
    e='#fb9a99'
    col = [a,b,c]
    col2=[d,e]
    linewidth = 2.5
    line_labels = T

    # for i in range(len(A1)):
    #     l[i] = ax1.step(t, A1[i], col[i], linewidth=linewidth, marker='o',where='post')[0]
    #        l[i] = ax1.plot(t, A1[i], col[i],marker='o',fillstyle='full' ,alpha=0.5)[0]

    # Pnftl
    l[0] = ax1.step(df['t'], df['P_NFTL'], col[0], linewidth=linewidth, marker='o',where='post')[0]
    l[1] = ax1.step(df_mon['t'], df_mon['Pmon_NFTL'], col[1], linewidth=linewidth, marker='o',where='post')[0]
    # l[2] = ax1.step(df['t'], df['P_Flex3'], col[2], linewidth=linewidth, marker='o',where='post')[0]
    # l[3] = ax1.step(df_mon['t'], df_mon['Pmon_Flex3'], col2[0], linewidth=linewidth, marker='o',where='post')[0]



    # for ii in range(len(A2)):
    #     ll[ii]=ax2.step(t2, A2[ii], col2[ii],linewidth=linewidth, marker='o',where='post')

    # Pcee
    ll[0] = ax2.step(df['t'], df['P_EV'], col2[0],linewidth=linewidth, marker='o',where='post')[0]
    ll[1] = ax2.step(df['t'], df['P_TES'], col2[1],linewidth=linewidth, marker='o',where='post')[0]

    ax1.set(ylabel=('Power [kW]'))
    ax1.set_ylabel('Power [kW]',fontsize=20)
    ax1.tick_params(labelsize=15)
    ax2.set_xlabel('Time [h:m]',fontsize=20)
    ax2.set_ylabel('Power [kW]',fontsize=20)
    ax2.tick_params(labelsize=15)
    
    ax1.xaxis.set_ticks_position('both')
    ax1.yaxis.set_ticks_position('both')
    ax2.xaxis.set_ticks_position('both')
    ax2.yaxis.set_ticks_position('both')

    ax1.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   #to get a tick every 15 minutes
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))     #optional formatting 

    ax2.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   #to get a tick every 15 minutes
    ax2.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))     #optional formatting 

    ax1.tick_params(direction='in',axis='both', color='black',labelsize=18,length = 6, width = 1)
    # ax1.tick_params(direction='in',which='minor', axis='both', color='black',length = 4, width = 1)
    ax2.tick_params(direction='in', axis='both', color='black', labelsize=18, length=6, width=1)
    # ax2.tick_params(direction='in', which='minor', axis='both', color='black', length=4, width=1)

    ax1.legend([l[0], l[1], l[2]], labels=line_labels[:3],loc='center right', prop={'size': 16})
    ax2.legend([ll[0], ll[1]], labels=line_labels[3:],loc='center right', prop={'size': 16})

    ax1.grid(b=None, which='major', axis='both',alpha=0.5)
    ax2.grid(b=None, which='major', axis='both',alpha=0.5)
    # ax1.set_xlim([t[0],t[-1]])
    #ax1.set_ylim=
    # ax2.set_xlim([t[0],t[-1]])
    #ax2.set_ylim=

    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(18.5, 10.5, forward=True)

    matplotlib.rc('font', **font)

    plt.show()

def plot_1d_opterror(df, df_mon):
    num = 2
    T = ['$P_{monitor, NFL}$', '$P_{controller, NFL}$','$P_{NFL}$','$P_{monitor,CEE}$','$P_{controller, TES}$','$P_{TES}$']
    l = [None] * 4
    ll = [None] *2

    # x_lim_min=n.values
    # x_lim_min=x_lim_min[0]
    # x_lim_max=n.values
    # x_lim_max =x_lim_max[-1]

    fig, (ax1,ax2) = plt.subplots(num,1,sharex=True)

    # t = t.data.to_pydatetime()

    #ax = plt.gca()

    font = {'family': 'serif', 'serif': 'Computer Modern Roman',
            'size': 16}
    #t=pd.to_datetime(n).dt.time
    #t=n

    a= '#33a02c'
    b= '#1f78b4'
    c= '#e31a1c'
    d='#6a3d9a'
    e='#fb9a99'
    col = [a,b,c]
    col2=[d,e]
    linewidth = 2.5
    line_labels = T

    Pmon_NFL = df_mon['Pmon_NFTL'] + df_mon['Pmon_Flex3'] + df_mon['Pmon_Flex1']
    P_NFL = df['P_NFTL'] + df['P_Flex3'] + df['P_Flex1']

    # for i in range(len(A1)):
    #     l[i] = ax1.step(t, A1[i], col[i], linewidth=linewidth, marker='o',where='post')[0]
    #        l[i] = ax1.plot(t, A1[i], col[i],marker='o',fillstyle='full' ,alpha=0.5)[0]

    # Pnftl
    l[1] = ax1.step(df_mon['t'], Pmon_NFL, col[1], linewidth=linewidth, marker='o',where='post')[0]
    l[0] = ax1.step(df['t'], P_NFL, col[0], linewidth=linewidth, marker='o',where='post')[0]
    # l[2] = ax1.step(df['t'], df['P_Flex3'], col[2], linewidth=linewidth, marker='o',where='post')[0]
    # l[3] = ax1.step(df_mon['t'], df_mon['Pmon_Flex3'], col2[0], linewidth=linewidth, marker='o',where='post')[0]



    # for ii in range(len(A2)):
    #     ll[ii]=ax2.step(t2, A2[ii], col2[ii],linewidth=linewidth, marker='o',where='post')

    # Pcee
    ll[0] = ax2.step(df_mon['t'], df_mon['Pmon_CEE'], col2[0],linewidth=linewidth, marker='o',where='post')[0]
    # ll[1] = ax2.step(df['t'], df['P_TES'], col2[1],linewidth=linewidth, marker='o',where='post')[0]

    ax1.set(ylabel=('Power [kW]'))
    ax1.set_ylabel('Power [kW]',fontsize=20)
    ax1.tick_params(labelsize=15)
    ax2.set_xlabel('Time [h:m]',fontsize=20)
    ax2.set_ylabel('Power [kW]',fontsize=20)
    ax2.tick_params(labelsize=15)
    
    ax1.xaxis.set_ticks_position('both')
    ax1.yaxis.set_ticks_position('both')
    ax2.xaxis.set_ticks_position('both')
    ax2.yaxis.set_ticks_position('both')

    ax1.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   #to get a tick every 15 minutes
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))     #optional formatting 

    ax2.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   #to get a tick every 15 minutes
    ax2.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))     #optional formatting 

    ax1.tick_params(direction='in',axis='both', color='black',labelsize=18,length = 6, width = 1)
    # ax1.tick_params(direction='in',which='minor', axis='both', color='black',length = 4, width = 1)
    ax2.tick_params(direction='in', axis='both', color='black', labelsize=18, length=6, width=1)
    # ax2.tick_params(direction='in', which='minor', axis='both', color='black', length=4, width=1)

    ax1.legend([l[0], l[1], l[2]], labels=line_labels[:3],loc='center right', prop={'size': 16})
    ax2.legend([ll[0], ll[1]], labels=line_labels[3:],loc='center right', prop={'size': 16})

    ax1.grid(b=None, which='major', axis='both',alpha=0.5)
    ax2.grid(b=None, which='major', axis='both',alpha=0.5)
    # ax1.set_xlim([t[0],t[-1]])
    #ax1.set_ylim=
    # ax2.set_xlim([t[0],t[-1]])
    #ax2.set_ylim=

    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(18.5, 10.5, forward=True)

    matplotlib.rc('font', **font)

    plt.show()

def plot_1d_linelimit(df, df_mon):
    num = 2
    T = ['$P_{monitor, NFTL}$','$P_{monitor, Cable}$', '$P_{controller, Cable}$','$P_{controller,CEE}$','$P_{controller, TES}$','$P_{TES}$']
    l = [None] * 4
    ll = [None] *2

    # x_lim_min=n.values
    # x_lim_min=x_lim_min[0]
    # x_lim_max=n.values
    # x_lim_max =x_lim_max[-1]

    fig, (ax1,ax2) = plt.subplots(num,1,sharex=True)

    # t = t.data.to_pydatetime()

    #ax = plt.gca()

    font = {'family': 'serif', 'serif': 'Computer Modern Roman',
            'size': 16}
    #t=pd.to_datetime(n).dt.time
    #t=n

    a= '#33a02c'
    b= '#1f78b4'
    c= '#e31a1c'
    d='#6a3d9a'
    e='#fb9a99'
    col = [a,b,c]
    col2=[d,e]
    linewidth = 2.5
    line_labels = T

    Pmon_Cable = df_mon['Pmon_NFTL'] + df_mon['Pmon_Flex3'] + df_mon['Pmon_CEE']
    P_Cable = df['P_NFTL'] + df['P_Flex3'] + df['P_TES'] + df['P_EV']
    P_Sto = df['P_TES'] + df['P_EV']

    # for i in range(len(A1)):
    #     l[i] = ax1.step(t, A1[i], col[i], linewidth=linewidth, marker='o',where='post')[0]
    #        l[i] = ax1.plot(t, A1[i], col[i],marker='o',fillstyle='full' ,alpha=0.5)[0]

    # Pnftl
    l[2] = ax1.step(df_mon['t'], df_mon['Pmon_NFTL'], col[2], linewidth=linewidth, marker='o',where='post')[0]
    l[1] = ax1.step(df_mon['t'], Pmon_Cable, col[1], linewidth=linewidth, marker='o',where='post')[0]
    l[0] = ax1.step(df['t'], P_Cable, col[0], linewidth=linewidth, marker='o',where='post')[0]
    # l[3] = ax1.step(df_mon['t'], df_mon['Pmon_Flex3'], col2[0], linewidth=linewidth, marker='o',where='post')[0]



    # for ii in range(len(A2)):
    #     ll[ii]=ax2.step(t2, A2[ii], col2[ii],linewidth=linewidth, marker='o',where='post')

    # Pcee
    ll[0] = ax2.step(df['t'], P_Sto, col2[0],linewidth=linewidth, marker='o',where='post')[0]
    # ll[1] = ax2.step(df['t'], df['P_TES'], col2[1],linewidth=linewidth, marker='o',where='post')[0]

    ax1.set(ylabel=('Power [kW]'))
    ax1.set_ylabel('Power [kW]',fontsize=20)
    ax1.tick_params(labelsize=15)
    ax2.set_xlabel('Time [h:m]',fontsize=20)
    ax2.set_ylabel('Power [kW]',fontsize=20)
    ax2.tick_params(labelsize=15)
    
    ax1.xaxis.set_ticks_position('both')
    ax1.yaxis.set_ticks_position('both')
    ax2.xaxis.set_ticks_position('both')
    ax2.yaxis.set_ticks_position('both')

    ax1.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   #to get a tick every 15 minutes
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))     #optional formatting 

    ax2.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   #to get a tick every 15 minutes
    ax2.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))     #optional formatting 

    ax1.tick_params(direction='in',axis='both', color='black',labelsize=18,length = 6, width = 1)
    # ax1.tick_params(direction='in',which='minor', axis='both', color='black',length = 4, width = 1)
    ax2.tick_params(direction='in', axis='both', color='black', labelsize=18, length=6, width=1)
    # ax2.tick_params(direction='in', which='minor', axis='both', color='black', length=4, width=1)

    ax1.legend([l[0], l[1], l[2]], labels=line_labels[:3],loc='center right', prop={'size': 16})
    ax2.legend([ll[0], ll[1]], labels=line_labels[3:],loc='center right', prop={'size': 16})

    ax1.grid(b=None, which='major', axis='both',alpha=0.5)
    ax2.grid(b=None, which='major', axis='both',alpha=0.5)
    # ax1.set_xlim([t[0],t[-1]])
    #ax1.set_ylim=
    # ax2.set_xlim([t[0],t[-1]])
    #ax2.set_ylim=

    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(18.5, 10.5, forward=True)

    matplotlib.rc('font', **font)

    plt.show()


def plot_1d(n, A1, A2, num=2):
    T = ['$P_{NFP}$','$P_{NFL}$','$P_{grid}$','$P_{EV}$','$P_{TES}$']
    l = [None] * len(A1)
    ll = [None] * len(A2)


    # x_lim_min=n.values
    # x_lim_min=x_lim_min[0]
    # x_lim_max=n.values
    # x_lim_max =x_lim_max[-1]

    fig, (ax1,ax2) = plt.subplots(num,1,sharex=True)
    # ax1.set_xlim([x_lim_min,x_lim_max])
    t=n
    #t=t.strftime("%H-%M-%S")
    #t=t.dt.strftime("%H:%M:%S")

    # t = t.data.to_pydatetime()

    #ax = plt.gca()

    font = {'family': 'serif', 'serif': 'Computer Modern Roman',
            'size': 16}
    #t=pd.to_datetime(n).dt.time
    #t=n


    a= '#33a02c'
    b= '#1f78b4'
    c= '#e31a1c'
    d='#6a3d9a'
    e='#fb9a99'
    col = [a,b,c]
    col2=[d,e]
    linewidth = 2.5
    line_labels = T

    for i in range(len(A1)):
        l[i] = ax1.step(t, A1[i], col[i], linewidth=linewidth, marker='o',where='post')[0]
    #        l[i] = ax1.plot(t, A1[i], col[i],marker='o',fillstyle='full' ,alpha=0.5)[0]

    ax1.set(ylabel=('Power [kW]'))

    for ii in range(len(A2)):
        ll[ii]=ax2.step(t, A2[ii], col2[ii],linewidth=linewidth, marker='o',where='post')
    ax1.set_ylabel('Power [kW]',fontsize=20)
    ax1.tick_params(labelsize=15)
    ax2.set_xlabel('Time [h:m]',fontsize=20)
    ax2.set_ylabel('Power [kW]',fontsize=20)
    ax2.tick_params(labelsize=15)
    
    ax1.xaxis.set_ticks_position('both')
    ax1.yaxis.set_ticks_position('both')
    ax2.xaxis.set_ticks_position('both')
    ax2.yaxis.set_ticks_position('both')

    ax1.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   #to get a tick every 15 minutes
    ax1.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))     #optional formatting 

    ax1.tick_params(direction='in',axis='both', color='black',labelsize=18,length = 6, width = 1)
    # ax1.tick_params(direction='in',which='minor', axis='both', color='black',length = 4, width = 1)
    ax2.tick_params(direction='in', axis='both', color='black', labelsize=18, length=6, width=1)
    # ax2.tick_params(direction='in', which='minor', axis='both', color='black', length=4, width=1)

    ax1.legend([l[0], l[1], l[2]], labels=line_labels[:3],loc='center right', prop={'size': 16})
    ax2.legend([ll[0], ll[1]], labels=line_labels[3:],loc='center right', prop={'size': 16})

    ax1.grid(b=None, which='major', axis='both',alpha=0.5)
    ax2.grid(b=None, which='major', axis='both',alpha=0.5)
    # ax1.set_xlim([t[0],t[-1]])
    #ax1.set_ylim=
    # ax2.set_xlim([t[0],t[-1]])
    #ax2.set_ylim=

    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(18.5, 10.5, forward=True)

    matplotlib.rc('font', **font)

    plt.show()

def error(x, y):
    error_v  = abs(x - y)
    error = error_v.sum()
    error = error/len(error_v)
    return error_v, error

def plot_1d_SGE(y, x = None, lgd = None):
    style =['k','r','k--','r--','g','g--']
    a= '#e41a1c'
    b= '#377eb8'
    c= '#4daf4a'
    style = [b,c,a]
    font = {'family': 'serif', 'serif': 'Computer Modern Roman',
            'size': 16}
    fig = plt.figure()
    ax = fig.gca()

    labels=['$P_{mis}$','$P_{sto}$','$P_{loss}$']
    #labels = ['$T^t(x) - Modèle\; physique$','$T^t(x) - Modèle\; simple$','$T^{t+\Delta t}(x) - Modèle \;physique$','$T^{t+\Delta t}(x) - Modèle\; simple$']
    #labels = ['$T^t(x) - Modèle\; physique$', '$T^{t+\Delta t}(x) - Modèle \;physique$']

    if x == None :
        x = np.arange(0, 240, 1)
    for i in range(len(y)):
        lines = plt.plot(x,y[i],style[i], label=labels[i])
        plt.setp(lines, linewidth=3)
        plt.legend(loc='lower left', prop={'size':16})

    ax.xaxis.set_ticks_position('both')
    ax.yaxis.set_ticks_position('both')
    ax.set_xticks(np.arange(0,240,20), minor=True)
    ax.set_yticks(np.arange(-3,3,0.25), minor=True)
    ax.set_xlim([0, 240])

    ax.set_xlabel('Temps écoulé (h)', fontsize= 18)
    ax.set_ylabel('Puissance thermique (MW)', fontsize=18)

    #ax.set_xlabel('Distance par rapport à l\'entrée chaude du stockage (m)', fontsize=18)
    #ax.set_ylabel('Température de stockage (°C)', fontsize=18)

    ax.tick_params(direction='in',axis='both', color='black',labelsize=18,length = 8, width = 1)
    ax.tick_params(direction='in',which='minor', axis='both', color='black',length = 4, width = 1)
    #ax.xaxis.set_tick_params(direction='in', which='top')
    #ax.yaxis.set_tick_params(direction='in', which='right')

    matplotlib.rc('font', **font)

    plt.show()

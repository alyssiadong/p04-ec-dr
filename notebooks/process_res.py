import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime 
import matplotlib.dates as mdates
from scipy import integrate
import plot_functions

class ProcessRes():

	def __init__(self, filename):

		self.filepath = 'results/' + filename
		self.filepathmonitor = 'results/monitor_' + filename
		self.df = pd.read_csv(self.filepath)
		self.df = self.df.drop([0])
		self.df_mon = pd.read_csv(self.filepathmonitor)
		self.df_mon = self.df_mon.drop([0])

		self.df['t'] = pd.to_datetime(self.df['datetime'], format = '%Y-%m-%d_%H-%M-%S')
		self.df_mon['t'] = pd.to_datetime(self.df_mon['datetime'], format = '%Y-%m-%d_%H-%M-%S')

		self.df['t_sec'] = self.df['t'].apply(lambda x:x.hour) *3600 \
						+ self.df['t'].apply(lambda x:x.minute) *60 \
						+ self.df['t'].apply(lambda x:x.second)
		self.df_mon['t_sec'] = self.df_mon['t'].apply(lambda x:x.hour) *3600\
						+ self.df_mon['t'].apply(lambda x:x.minute) *60\
						+ self.df_mon['t'].apply(lambda x:x.second)

		self.df['t_hour'] = self.df['t_sec']/3600
		self.df_mon['t_hour'] = self.df_mon['t_sec']/3600

		self.df_mon['Pmon_eq'] = self.df_mon['Pmon_PV'] + \
							self.df_mon['Pmon_Gaia'] +\
							self.df_mon['Pmon_NFTL'] +\
							self.df_mon['Pmon_Flex1'] +\
							self.df_mon['Pmon_Flex3'] +\
							self.df_mon['Pmon_Grid'] +\
							self.df_mon['Pmon_CEE'] 

		self.df['P_add'] = self.df['P_NFTL'] + self.df['P_Grid']
		self._opposite()
		self._no_controller_case()
		self._data_processing()

	def _opposite(self):

		self.df_mon['Pmon_PV'] = -self.df_mon['Pmon_PV']
		self.df_mon['Pmon_Gaia'] = -self.df_mon['Pmon_Gaia']
		self.df_mon['Pmon_NFTL'] = -self.df_mon['Pmon_NFTL']
		self.df_mon['Pmon_Flex1'] = -self.df_mon['Pmon_Flex1']
		self.df_mon['Pmon_Flex3'] = -self.df_mon['Pmon_Flex3']
		self.df_mon['Pmon_Grid'] = -self.df_mon['Pmon_Grid']
		self.df_mon['Pmon_CEE'] = -self.df_mon['Pmon_CEE']
		self.df_mon['Pmon_eq'] = -self.df_mon['Pmon_eq']
		self.df_mon['Pmon_Cable'] = -self.df_mon['Pmon_Cable']
		
		self.P_NFP = self.df['P_Gaia'] + self.df['P_PV']
		self.P_NFL= self.df['P_Flex3'] + self.df['P_NFTL'] + self.df['P_Flex1']
		self.P_grid = self.df['P_Grid']
		self.P_EV=self.df['P_EV']
		self.P_TES = self.df['P_TES']
		self.P_CEE = self.df_mon['Pmon_CEE']

	# Process data functions
	def _no_controller_case(self):

		N = len(self.df.index)
		P_EV = [7] * N

		self.df_nc = pd.DataFrame(
			{
				't' : self.df['t'],
				't_sec' : self.df['t_sec'],
				't_hour' : self.df['t_hour'],
				'datetime' : self.df['datetime'],
				'P_PV' : self.df['P_PV'],
				'P_Gaia' : self.df['P_Gaia'],
				'P_Flex1' : self.df['P_Flex1'],
				'P_Flex3' : self.df['P_Flex3'],
				'P_NFTL' : self.df['P_NFTL'],
				'P_EV' : P_EV,
			}
			)
		self.df_nc['P_Grid'] = - ( self.df_nc['P_PV'] \
								+ self.df_nc['P_Gaia']  \
								+ self.df_nc['P_Flex1']  \
								+ self.df_nc['P_Flex3']  \
								+ self.df_nc['P_NFTL']  \
								+ self.df_nc['P_EV']  )
		self.df_nc['P_Cable'] = ( self.df_nc['P_Flex3']  \
								+ self.df_nc['P_NFTL']  \
								+ self.df_nc['P_EV']  )
		self.df_nc['SoC_EV'] = integrate.cumtrapz(self.df_nc['P_EV'], 
						x = self.df_nc['t_hour'], 
						initial = 0) +  self.df['SoC_EV'].values[0]

		N = len(self.df_mon.index)
		P_EV = [7] * N

		self.df_mon_nc = pd.DataFrame(
			{
				't' : self.df_mon['t'],
				't_sec' : self.df_mon['t_sec'],
				't_hour' : self.df_mon['t_hour'],
				'datetime' : self.df_mon['datetime'],
				'P_PV' : self.df_mon['Pmon_PV'],
				'P_Gaia' : self.df_mon['Pmon_Gaia'],
				'P_Flex1' : self.df_mon['Pmon_Flex1'],
				'P_Flex3' : self.df_mon['Pmon_Flex3'],
				'P_NFTL' : self.df_mon['Pmon_NFTL'],
				'P_EV' : P_EV,
			}
			)
		self.df_mon_nc['P_Grid'] = - ( self.df_mon_nc['P_PV'] \
								+ self.df_mon_nc['P_Gaia']  \
								+ self.df_mon_nc['P_Flex1']  \
								+ self.df_mon_nc['P_Flex3']  \
								+ self.df_mon_nc['P_NFTL']  \
								+ self.df_mon_nc['P_EV']  )
		self.df_mon_nc['P_Cable'] = ( self.df_mon_nc['P_Flex3']  \
								+ self.df_mon_nc['P_NFTL']  \
								+ self.df_mon_nc['P_EV']  )
		self.df_mon_nc['SoC_EV'] = integrate.cumtrapz(self.df_mon_nc['P_EV'], 
						x = self.df_mon_nc['t_hour'], 
						initial = 0) +  self.df['SoC_EV'].values[0]

	def _data_processing(self):
		cost = 0.39 # DKK/kWh
		grid_balance = self.df_mon['Pmon_Grid']
		grid_imported = grid_balance.apply(lambda x:x if x<0 else 0)
		grid_exported = grid_balance.apply(lambda x:x if x>0 else 0)

		self.energy_imported = -integrate.trapz(grid_imported, x=self.df_mon['t_hour'])
		self.energy_exported = integrate.trapz(grid_exported, x=self.df_mon['t_hour'])
		self.energy_bill = cost*self.energy_imported
		self.maximum_infeed = np.abs(grid_balance).max()
		self.pv_generated = -integrate.trapz(self.df_mon['Pmon_PV'], x=self.df_mon['t_hour'])
		self.gaia_generated = -integrate.trapz(self.df_mon['Pmon_Gaia'], x=self.df_mon['t_hour'])
		self.locally_generated = self.pv_generated + self.gaia_generated
		self.ev_charge = integrate.trapz(self.df['P_EV'], x=self.df['t_hour'])
		self.tes_charge = integrate.trapz(self.df['P_TES'], x=self.df['t_hour'])
		self.self_consumption_index = (self.pv_generated+self.gaia_generated-self.energy_exported)/(self.pv_generated+self.gaia_generated)

		grid_balance = self.df_mon_nc['P_Grid']
		grid_imported = grid_balance.apply(lambda x:x if x<0 else 0)
		grid_exported = grid_balance.apply(lambda x:x if x>0 else 0)

		self.energy_imported_nc = -integrate.trapz(grid_imported, x=self.df_mon_nc['t_hour'])
		self.energy_exported_nc = integrate.trapz(grid_exported, x=self.df_mon_nc['t_hour'])
		self.energy_bill_nc = cost*self.energy_imported_nc
		self.maximum_infeed_nc = np.abs(grid_balance).max()
		self.pv_generated_nc = -integrate.trapz(self.df_mon_nc['P_PV'], x=self.df_mon_nc['t_hour'])
		self.gaia_generated_nc = -integrate.trapz(self.df_mon_nc['P_Gaia'], x=self.df_mon_nc['t_hour'])
		self.locally_generated_nc = self.pv_generated_nc + self.gaia_generated_nc
		self.ev_charge_nc = integrate.trapz(self.df_nc['P_EV'], x=self.df_nc['t_hour'])
		self.tes_charge_nc = 0
		self.self_consumption_index_nc = (self.pv_generated_nc+self.gaia_generated_nc-self.energy_exported_nc)/(self.pv_generated_nc+self.gaia_generated_nc)

		self.data = {
					'type' : ['with controller', 'without controller'],
					'Energy bill [DKK/kWh]' : [self.energy_bill, self.energy_bill_nc],
					'Max. in-feed [kW]' : [self.maximum_infeed, self.maximum_infeed_nc],
					'Energy Imported [kWh]' : [self.energy_imported, self.energy_imported_nc],
					'Energy exported [kWh]' : [self.energy_exported, self.energy_exported_nc],
					'Self consumption index' : [self.self_consumption_index, self.self_consumption_index_nc],
					'Energy locally generated (PV+WT) [kWh]' : [self.locally_generated, self.locally_generated_nc],
					'EV battery charge [kWh]' : [self.ev_charge, self.ev_charge_nc],
					'TES charge [kWh]' : [self.tes_charge, self.tes_charge_nc],
		}

		self.data = pd.DataFrame(data =self.data)

	def processing_cable_limitation(self):
		power_cable = self.df_mon['Pmon_Cable']
		self.df_mon['CableLimitCompliance'] = power_cable.apply(lambda x:1 if x<=10.1 else 0)
		self.df_mon['CableLimitViolation'] = power_cable.apply(lambda x:1 if x>10.1 else 0)

		self.cableLimitComplianceDuration = integrate.trapz(self.df_mon['CableLimitCompliance'], x=self.df_mon['t_sec'])
		self.cableLimitViolationDuration = integrate.trapz(self.df_mon['CableLimitViolation'], x=self.df_mon['t_sec'])
		self.cableLimitComplianceRatio = self.cableLimitComplianceDuration/(self.cableLimitViolationDuration + self.cableLimitComplianceDuration)		
		self.cablePowerMax = power_cable.max()

		power_cable = self.df_mon_nc['P_Cable']
		self.df_mon_nc['CableLimitCompliance'] = power_cable.apply(lambda x:1 if x<=10.1 else 0)
		self.df_mon_nc['CableLimitViolation'] = power_cable.apply(lambda x:1 if x>10.1 else 0)

		self.cableLimitComplianceDuration_nc = integrate.trapz(self.df_mon_nc['CableLimitCompliance'], x=self.df_mon_nc['t_sec'])
		self.cableLimitViolationDuration_nc = integrate.trapz(self.df_mon_nc['CableLimitViolation'], x=self.df_mon_nc['t_sec'])
		self.cableLimitComplianceRatio_nc = self.cableLimitComplianceDuration_nc/(self.cableLimitViolationDuration_nc + self.cableLimitComplianceDuration_nc)		
		self.cablePowerMax_nc = power_cable.max()

		self.data.insert(1, "Cable limit compliance duration (s)", [self.cableLimitComplianceDuration, self.cableLimitComplianceDuration_nc])
		self.data.insert(2, "Cable limit violation duration (s)", [self.cableLimitViolationDuration, self.cableLimitViolationDuration_nc])
		self.data.insert(3, "Cable limit compliance ratio", [self.cableLimitComplianceRatio, self.cableLimitComplianceRatio_nc])
		self.data.insert(4, "Maximum cable power (kW)", [self.cablePowerMax, self.cablePowerMax_nc])

	# Plotting functions
	def plot_power(self):
		fig, ax = plt.subplots()

		ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   
		ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))    

		self.df.plot(x = 't', 
			y = [
				'P_PV',
				'P_Gaia',
				'P_EV', 
            	'P_TES',
            	'P_NFTL',
            	'P_Flex1',
            	'P_Flex3',
            	'P_Grid'
            	], 
            	ax=ax,
            	title = 'With controller'
            	)

		ax.legend(bbox_to_anchor=(1.1, 1.05))
		plt.show()

	def plot_power_nc(self):
		fig, ax = plt.subplots()

		ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   
		ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))    

		self.df_nc.plot(x = 't', 
			y = [
				'P_PV',
				'P_Gaia',
				'P_EV', 
            	'P_NFTL',
            	'P_Flex1',
            	'P_Flex3',
            	'P_Grid'
            	], 
            	ax=ax,
            	title = 'Without controller'
            	)

		ax.legend(bbox_to_anchor=(1.1, 1.05))
		plt.show()

	def plot_power_monitor(self):
		fig, ax = plt.subplots()

		ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   
		ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))    

		self.df_mon.plot(x = 't', 
			y = [
				# 'P_add',
				'Pmon_PV',
				'Pmon_Gaia',
				'Pmon_CEE', 
            	'Pmon_NFTL',
            	'Pmon_Flex1',
            	'Pmon_Flex3',
            	'Pmon_Grid',
            	'Pmon_Cable'
            	], 
            	ax=ax,
            	title = 'Monitor'
            	)

		ax.legend(bbox_to_anchor=(1.1, 1.05))
		plt.show()

	def plot_power_monitor_nc(self):
		fig, ax = plt.subplots()

		ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   
		ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))    

		self.df_mon_nc.plot(x = 't', 
			y = [
				# 'P_add',
				'P_PV',
				'P_Gaia',
				'P_EV',
            	'P_NFTL',
            	'P_Flex1',
            	'P_Flex3',
            	'P_Grid',
            	'P_Cable'
            	], 
            	ax=ax,
            	title = 'Monitor, without controller'
            	)

		ax.legend(bbox_to_anchor=(1.1, 1.05))
		plt.show()

	def plot_SoC(self):
		fig, ax = plt.subplots()

		color = 'tab:blue'
		self.df.plot(x = 't', 
			y = [
				'SoC_EV', 
            	], ax=ax, color = color)
		ax2 = ax.twinx()  # instantiate a second axes that shares the same x-axis

		color2 = 'tab:red'
		self.df.plot(x = 't', 
			y = [
				'SoC_TES', 
            	], ax=ax2, color = color2,
            	title = 'With controller')

		ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   
		ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))   
		ax2.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   
		ax2.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))    

		ax.legend(loc = 'upper left')
		ax2.legend(loc = 'lower right')

		ax.tick_params(axis='y', labelcolor=color)
		ax2.tick_params(axis='y', labelcolor=color2)

		plt.show()

	def plot_SoC_nc(self):
		fig, ax = plt.subplots()

		color = 'tab:blue'
		self.df_nc.plot(x = 't', 
			y = [
				'SoC_EV', 
            	], ax=ax, color = color)
		
		ax.xaxis.set_major_locator(mdates.MinuteLocator(interval=1))   
		ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))    

		ax.legend(loc = 'upper left')

		ax.tick_params(axis='y', labelcolor=color)

		plt.show()

	def plot_cable_limit(self):
		plot_functions.plot_1d_linelimit(self.df, self.df_mon)

	def plot_opterror(self):
		plot_functions.plot_1d_opterror(self.df, self.df_mon)

	def plot_comerror(self):
		plot_functions.plot_1d_comerror(self.df, self.df_mon)



if __name__ == "__main__":

	filename = '2020-01-31_08-54-24.csv'
	pr = ProcessRes(filename)

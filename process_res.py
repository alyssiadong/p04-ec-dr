import pandas as pd
import matplotlib.pyplot as plt
import plot_functions
import datetime 

class ProcessRes():

	def __init__(self, filename, filename_monitor):

		self.filepath = 'results/' + filename
		self.filepathmonitor = 'results/monitor_' + filename_monitor
		self.df = pd.read_csv(self.filepath)
		self.df = self.df.drop([0])
		self.df_mon = pd.read_csv(self.filepathmonitor)
		self.df_mon = self.df_mon.drop([0])
		self.P_NFTL = self.df['P_NFTL']
		self.P_Flex3 = self.df['P_Flex3']
		self.P_NFL = None
		self.P_NFP = None
		self.P_grid = None
		self.P_EV = None
		self.P_TES = None
		self.t =None

		self.df['t'] = pd.to_datetime(self.df['datetime'], format = '%Y-%m-%d_%H-%M-%S')
		self.df_mon['t'] = pd.to_datetime(self.df_mon['datetime'], format = '%Y-%m-%d_%H-%M-%S')

		self.df_mon['Pmon_eq'] = self.df_mon['Pmon_PV'] + \
							self.df_mon['Pmon_Gaia'] +\
							self.df_mon['Pmon_NFTL'] +\
							self.df_mon['Pmon_Flex1'] +\
							self.df_mon['Pmon_Flex3'] +\
							self.df_mon['Pmon_Grid'] +\
							self.df_mon['Pmon_CEE'] 

		self.df['P_add'] = self.df['P_NFTL'] + self.df['P_Grid']
		self.opposite()
		plot_functions.plot_1d_linelimit(self.df, self.df_mon)

	def opposite(self):
		# self.df['P_PV'] = -self.df['P_PV']
		# self.df['P_Gaia'] = -self.df['P_Gaia']
		# self.df['P_EV'] = -self.df['P_EV']
		# self.df['P_TES'] = -self.df['P_TES']
		# self.df['P_NFTL'] = -self.df['P_NFTL']
		# self.df['P_Flex1'] = -self.df['P_Flex1']
		# self.df['P_Flex3'] = -self.df['P_Flex3']
		# self.df['P_Grid'] = -self.df['P_Grid']

		self.df_mon['Pmon_PV'] = -self.df_mon['Pmon_PV']
		self.df_mon['Pmon_Gaia'] = -self.df_mon['Pmon_Gaia']
		self.df_mon['Pmon_NFTL'] = -self.df_mon['Pmon_NFTL']
		self.df_mon['Pmon_Flex1'] = -self.df_mon['Pmon_Flex1']
		self.df_mon['Pmon_Flex3'] = -self.df_mon['Pmon_Flex3']
		self.df_mon['Pmon_Grid'] = -self.df_mon['Pmon_Grid']
		self.df_mon['Pmon_CEE'] = -self.df_mon['Pmon_CEE']
		self.df_mon['Pmon_eq'] = -self.df_mon['Pmon_eq']
		self.df_mon['Pmon_Cable'] = -self.df_mon['Pmon_Cable']
		
		self.P_NFP = self.df['P_Gaia'] + self.df['P_PV']
		self.P_NFL= self.df['P_Flex3'] + self.df['P_NFTL'] + self.df['P_Flex1']
		self.P_grid = self.df['P_Grid']
		self.P_EV=self.df['P_EV']
		self.P_TES = self.df['P_TES']
		self.P_CEE = self.df_mon['Pmon_CEE']
		# self.t = self.df['t'].strftime("%H-%M-%S")
		self.t = self.df['t']
		self.t2 = self.df_mon['t']
		
	def plot_power(self):
		self.df.plot(x = 't', 
			y = [
				# 'P_add',
				'P_PV',
				'P_Gaia',
				'P_EV', 
            	'P_TES',
            	'P_NFTL',
            	'P_Flex1',
            	'P_Flex3',
            	'P_Grid'
            	])

	def plot_SoC(self):
		self.df.plot(x = 't', 
			y = [
				'SoC_EV', 
            	'SoC_TES',
            	])

	def plot_mon(self):
		self.df_mon.plot(x = 't', 
			y = [
				'Pmon_PV',
				'Pmon_Gaia',
            	'Pmon_NFTL',
            	'Pmon_Flex1',
            	'Pmon_Flex3',
            	'Pmon_Grid',
            	'Pmon_Cable',
            	'Pmon_CEE', 
            	'Pmon_eq'
			])

if __name__ == "__main__":

	filename = '2020-01-31_08-54-24.csv'
	filename_mon = '2020-01-31_08-54-24.csv'
	pr = ProcessRes(filename, filename_mon)

	# pr.plot_power()
	# plt.show()
#	pr.plot_mon()
#	plt.show()
#	pr.plot_SoC()
#	plt.show()
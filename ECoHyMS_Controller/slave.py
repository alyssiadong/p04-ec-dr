'''
    slave.py
    Written by Ibrahim Al Asmi and Alyssia Dong for the 2020 DTU SYSLAB Course.

    Not used in the actual controller.
    Contains the slave part of all simulated systems.
'''

import pandas as pd
import logging
from xmlrpc.server import SimpleXMLRPCServer

 
# Initializing PV and NFTL data
store = pd.HDFStore("dtu_mosaik/signals.h5")
PV_0 = store['PV715_20180125']
Gaia_0 = store['/Gaia_20180510']
NFTL_0 = store['/flexhouse_20180219']
Flex1_0 = store['/flexhouse_20180224']
Flex3_0 = store['/flexhouse_20180227']

start = pd.Period('2018-01-25 00:00:00')
end = pd.Period('2018-01-25 23:59:59')
end2 = pd.Period('2018-01-25 23:59:58')
ind = pd.period_range(start = start, end = end)
ind2 = pd.period_range(start = start, end = end2)

PV_series = pd.Series(data = PV_0.data, index = ind)
Gaia_series = pd.Series(data = Gaia_0.data, index = ind)
NFTL_series = pd.Series(data = NFTL_0.data, index = ind2)
Flex1_series = pd.Series(data = Flex1_0.data, index = ind)
Flex3_series = pd.Series(data = Flex3_0.data, index = ind)

# Battery model taken from dtu-mosaik
class MyBattSim:
    def __init__(
            self,
            rated_capacity=10,
            rated_discharge_capacity=20,
            rated_charge_capacity=20,
            roundtrip_efficiency=0.96,
            initial_charge_rel=0.50,
            charge_change_rate=0.9,
            dt=1.0/(60*60)):
        """
            Battery simulator.
            Time is assumed to proceed in integer steps.
            @input:
                rated_capacity: Max volume of charge [kWh]
                rated_discharge_capacity: Max output power [kW]
                rated_charge_capacity: Max input power [kW]
                roundtrip_efficiency: Total roundtrip efficiency [0.0-1.0]
                initial_charge_rel: Charge at time 0 relative to max [0.0 - 1.0]
                charge_change_rate: Rate at which charge follows setpoint,
                    1.0= instant following. [0.0 - 1.0]
                dt: number of hours per time step [h] (default: 1 sec per time step)
        """
        self.rated_capacity = rated_capacity
        self.rated_discharge_capacity = rated_discharge_capacity
        self.rated_charge_capacity = rated_charge_capacity
        self.eta = roundtrip_efficiency
        self.alpha = charge_change_rate

        # Internal variables
        self.curtime = 0 # Time is assumed to step in integer steps
        self.dt = dt
        self._P = 0
        self._Pset = 0
        self._charge = initial_charge_rel * rated_capacity

        # Externally visible variables
        self.Pext = 0
        self.Psetext = 0
        self.SoC = self._charge
        self.relSoC = self._charge / self.rated_capacity

    def calc_val(self, t):
        """
        """
        assert type(t) is int
        assert t >= self.curtime, "Must step to time of after or at current time: {0}. Was asked to step to {1}".format(t, self.curtime)
        self._ext_to_int() # Translate external state to internal state
        for _ in range(t - self.curtime):
            self._do_state_update()
        self._int_to_ext() # Translate internal state to external state
        self.curtime = t

    def _do_state_update(self):
        # Current power will slowly rise towards setpoint
        P = self.alpha * self._Pset + (1 - self.alpha) * self._P
        self._P = self._limit_P(P)
        self._update_charge()

    def _limit_P(self, P):
        # Limit according to bounds
        P = min(max(P, -self.rated_discharge_capacity), self.rated_charge_capacity)
        # Limit to available charge, roundtrip eff. is applied to output
        P = min(
                max(P, -self._charge * self.eta / self.dt), # Cannot discharge more than we have
                (self.rated_capacity - self._charge)/ self.dt) # Cannot overfill
        return P

    def _update_charge(self):
        C = self._charge + self._P / (self.eta if self._P>0 else 1) * self.dt
        self._charge = min(
                max(C, 0),
                self.rated_capacity)

    def _ext_to_int(self):
        self._Pset = - self.Psetext

    def _int_to_ext(self):
        self.Pext = - self._P
        self.SoC = self._charge
        self.relSoC = self._charge / self.rated_capacity

    # Getter/setters for external users
    @property
    def P(self):
        return self.Pext

    @property
    def Pset(self):
        return self.Psetext

    @Pset.setter
    def Pset(self, newPset):
        self.Psetext = min(self.rated_discharge_capacity,
                max(newPset, -self.rated_charge_capacity))

# Initializing flags
PV_flag = 1
Gaia_flag = 1
NFTL_flag = 1
Flex1_flag = 1
Flex3_flag = 1
EV_flag = 1
TES_flag = 1
Cable_flag = 1

# Internal functions
def getTimestamp():
    t0 = pd.Timestamp.now()
    t1 = pd.Timestamp(year = 2018, month = 1, day = 25,\
        hour = t0.hour, minute = t0.minute, second = t0.second)
    return t1

def getSecond():
    t0 = pd.Timestamp.now()
    return t0.hour*3600 + t0.minute*60 + t0.second


# Initializing storage units
EV = MyBattSim()
EV.calc_val(getSecond())
TES = MyBattSim()
TES.calc_val(getSecond())

# External functions
def getActivePower(syst):
    # Read the power measurement of PV or NFTL

    logging.debug(f'Call received: getActivePower()')
    t1 = getTimestamp()
    if syst == "PV":
        return float(-PV_series[t1])
    elif syst == "Gaia":
        return float(-Gaia_series[t1])
    elif syst == "NFTL":
        return float(NFTL_series[t1])
    elif syst == "Flex1":
        return float(Flex1_series[t1])
    elif syst == "Flex3":
        return float(Flex3_series[t1])
    elif syst == "EV" :
        EV.calc_val(getSecond())
        return float(-EV.P)
    elif syst == "TES" :
        TES.calc_val(getSecond())
        return float(-TES.P)
    elif syst == "Cable":
        return float(Heatpump_series[t1]+Flex1_series[t1]+Flex3_series[t1]-EV.P)
    else:
        return 0

def setActivePower(syst, Pset):
    # Set the power of the storage elements
    logging.debug(f'Call received: setActivePower()')
    if syst == "EV":
        EV.calc_val(getSecond())
        EV.Pset = Pset
        return 1
    elif syst == "TES":
        TES.calc_val(getSecond())
        TES.Pset = Pset
        return 1
    else :
        return 0

def getSoC(syst):
    # Read the current state of charge of the storage elements
    logging.debug(f'Call received: getSoC()')
    if syst == "EV":
        EV.calc_val(getSecond())
        return float(EV.SoC)
    elif syst == "TES":
        TES.calc_val(getSecond())
        return float(TES.SoC)
    else :
        return 0

def test_syst(syst):
    if syst == "PV":
        return PV_flag
    elif syst == "Gaia":
        return Gaia_flag
    elif syst == "NFTL":
        return NFTL_flag
    elif syst == "Flex1":
        return Flex1_flag
    elif syst == "Flex3":
        return Flex3_flag
    elif syst == "EV" :
        return EV_flag
    elif syst == "TES" :
        return TES_flag
    elif syst == "Cable":
        return Cable_flag
    else:
        return 0


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    server = SimpleXMLRPCServer(('localhost', 9000), logRequests=True)
    # Register the function we are serving
    server.register_function(getActivePower, "getActivePower")
    server.register_function(setActivePower, "setActivePower")
    server.register_function(getSoC, "getSoC")
    server.register_function(test_syst, "test_syst")
    try:
        print("Use Control-C to exit")
        # Start serving our functions
        server.serve_forever()
    except KeyboardInterrupt:
        print("Exiting")



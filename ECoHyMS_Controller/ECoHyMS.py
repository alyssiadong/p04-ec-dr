'''
    ECoHyMS.py
    Written by Ibrahim Al Asmi and Alyssia Dong for the 2020 DTU SYSLAB Course.
    
    Starts the controller and save the actual measurements in a csv file.
'''

from StateMachine import SyslabController
import pandas as pd
from syslab import SwitchBoard
import threading

def monitor_syslab():
    t0 = pd.Timestamp.now()
    timename = t0.strftime('%Y-%m-%d_%H-%M-%S')
    filename = 'results/monitor_' + timename + '.csv'

    df = pd.DataFrame( [[ timename,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0,
                            0]],
        columns = [ 'datetime','Pmon_PV', 'Pmon_Gaia', 'Pmon_Heatpump', 
        'Pmon_Heatbooster', 'Pmon_NFTL', 'Pmon_Flex1','Pmon_Flex3', 
        'Pmon_Grid', 'Pmon_CEE', 'Pmon_Cable'])

    try :
        while True:
            t0 = pd.Timestamp.now()
            timename = t0.strftime('%Y-%m-%d_%H-%M-%S')

            if (t0.second % 10) == 0:
                print("Still monitoring...")

            # read power values
            res = read_values_syslab()
            # save into file
            P_PV, P_Gaia, P_Grid, P_Heatpump,\
                P_Heatbooster, P_Flex1, P_Flex3, P_CEE = res

            P_NFTL = P_Heatbooster + P_Heatpump
            P_Cable = P_CEE + P_Flex3 + P_NFTL

            df2 = pd.DataFrame( [[ timename,
                                    P_PV,
                                    P_Gaia,
                                    P_Heatpump,
                                    P_Heatbooster,
                                    P_NFTL,
                                    P_Flex1,
                                    P_Flex3,
                                    P_Grid,
                                    P_CEE,
                                    P_Cable]],
                columns = [ 'datetime','Pmon_PV', 'Pmon_Gaia', 'Pmon_Heatpump', 
                'Pmon_Heatbooster', 'Pmon_NFTL', 'Pmon_Flex1','Pmon_Flex3', 
                'Pmon_Grid', 'Pmon_CEE', 'Pmon_Cable'])

            df = df.append(df2, ignore_index=True)
            df.to_csv(filename)

            global stop_threads
            if stop_threads:
                break

            time.sleep(1)

    except KeyboardInterrupt:
        print("Monitoring stops.")

def read_values_syslab():
    P_PV = read_value_syslab("PV")
    P_Gaia = read_value_syslab("Gaia")
    P_Grid = read_value_syslab("Grid")
    P_Heatpump = read_value_syslab("Heatpump")
    P_Heatbooster = read_value_syslab("Heatbooster")
    P_Flex1 = read_value_syslab("Flex1")
    P_Flex3 = read_value_syslab("Flex3")
    P_CEE = read_value_syslab("CEE")

    res = [P_PV,P_Gaia,P_Grid,P_Heatpump,
        P_Heatbooster,P_Flex1,P_Flex3, P_CEE]

    return res

def read_value_syslab(syst):
    SB715 = SwitchBoard('715-2') # Gaia, PCC, PV, Flex1
    SB716 = SwitchBoard('716-2') # CEE, NFTL, Flex3

    bay_numb = {'PV': 3, 'Flex1':2, 'Grid':0, 'Gaia':1, 
        'Heatpump':6, 'Heatbooster':7, 'Flex3':5, 'CEE':0}
    if syst in ['Gaia', 'Grid', 'PV', 'Flex1']:
        power = SB715.getActivePower(bay_numb[syst])

    elif syst in ['CEE', 'Heatpump', 'Heatbooster', 'Flex3']:
        power = SB716.getActivePower(bay_numb[syst])
    else:
        raise Exception("syst doesn't correspond to any keyword.")

    return power._value

if __name__ == "__main__":
    stop_threads = False
    t = threading.Thread(target\
        =monitor_syslab,args=())
    t.start()

    try:
        sm = SyslabController(setPower = True)
    except:
        print('Interruption')
    stop_threads = True
    t.join() 
    print('thread killed') 
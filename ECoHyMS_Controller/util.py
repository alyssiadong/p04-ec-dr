'''
	util.py
	Written by Ibrahim Al Asmi and Alyssia Dong for the 2020 DTU SYSLAB Course.

	Contains functions to compute the ADMM peer-to-peer optimization.
	The solver used is OSQP: https://osqp.org/

'''

import numpy as np
import scipy.sparse as sp
import osqp 

def price_update(name, agents, trades, prices, rho):
	id_agent = agents[name]['id']
	id_parts = [agents[name2]['id'] for name2 in agents[name]['partners']]

	new_price = np.zeros((trades.shape[0], trades.shape[2]))

	new_price[id_parts,:] = prices[id_agent,id_parts,:] - rho/2\
	*(trades[id_agent,id_parts,:]+trades[id_parts,id_agent,:])
	return new_price

def osqp_solve(P,q,A,l,u):
	m = osqp.OSQP()
	m.setup(P=P, q=q, A=A, l=l, u=u, verbose=False)
	results = m.solve()
	return results.x

def trades_update(name, agents, trades, prices, rho):

	id_agent = agents[name]['id']
	id_parts = [agents[name2]['id'] for name2 in agents[name]['partners']]

	N_part = len(id_parts)
	N_time = trades.shape[2]

	new_trades = np.zeros((trades.shape[0],trades.shape[2]))
	if name == 'PCC':

		# P matrix
		p1 = np.full((N_part, N_part), 1)
		P1 = sp.block_diag([p1 for i in range(N_time)],'csc')
		# P1 = sp.identity(N_time*N_part, format='csc')
		P2 = rho/2*sp.identity(N_time*N_part)

		P = 2*(P1 + P2)

		# q vector
		q1 = -rho*((trades[id_agent,id_parts,:]-trades[id_parts,id_agent,:])/2 \
			+ prices[id_agent,id_parts,:]/rho)
		# grid penalization : pcc-pv and pcc-gaia
		id_pv = id_parts.index(agents['PV']['id'])
		id_gaia = id_parts.index(agents['Gaia']['id'])
		q1[id_pv,:] = q1[id_pv,:] - 200
		q1[id_gaia,:] = q1[id_gaia,:] - 200

		q = q1.flatten(order = 'F')

		# A matrix
		a1 = np.full(N_part,1)
		A = sp.block_diag([a1 for i in range(N_time)],'csc')

		# l vector
		l = np.full(N_time, -np.inf)

		# u vector 
		u = np.full(N_time, np.inf)
	elif name in ['PV', 'Gaia']:
		# P matrix
		P = rho*sp.identity(N_time*N_part, format='csc')

		# q vector
		q1 = -rho*((trades[id_agent,id_parts,:]-trades[id_parts,id_agent,:])/2 \
			+ prices[id_agent,id_parts,:]/rho)
		# grid penalization : pcc-pv and pcc-gaia
		id_pcc = id_parts.index(agents['PCC']['id'])
		q1[id_pcc,:] = q1[id_pcc,:] + 400
		q = q1.flatten(order = 'F')

		# A matrix
		a1 = np.full(N_part,1)
		A1 = sp.block_diag([a1 for i in range(N_time)],'csc')

		A2 = sp.identity(N_part*N_time)

		A = sp.vstack((A1,A2),format='csc')

		# l vector
		l1 = agents[name]['Ppred']
		l2 = np.repeat(l1, N_part)
		l = np.concatenate((l1,l2))

		# u vector 
		u1 = agents[name]['Ppred']
		u2 = np.zeros(N_part*N_time)
		u = np.concatenate((u1,u2))
	elif name in ['NFTL', 'Flex1', 'Flex3']:
		# P matrix
		P = rho*sp.identity(N_time*N_part, format='csc')

		# q vector
		q1 = -rho*((trades[id_agent,id_parts,:]-trades[id_parts,id_agent,:])/2 \
			+ prices[id_agent,id_parts,:]/rho)
		q = q1.flatten(order = 'F')

		# A matrix
		a1 = np.full(N_part,1)
		A1 = sp.block_diag([a1 for i in range(N_time)],'csc')

		A2 = sp.identity(N_part*N_time)

		A = sp.vstack((A1,A2),format='csc')

		# l vector
		l1 = agents[name]['Ppred']
		l2 = np.zeros(N_part*N_time)
		l = np.concatenate((l1,l2))

		# u vector 
		u1 = agents[name]['Ppred']
		u2 = np.repeat(u1, N_part)
		u = np.concatenate((u1,u2))
	elif name == 'EV':
		# P matrix
		P = rho*sp.identity(N_time*N_part, format='csc')

		# q vector
		q1 = -rho*((trades[id_agent,id_parts,:]-trades[id_parts,id_agent,:])/2 \
			+ prices[id_agent,id_parts,:]/rho)
		# encourage ev-cable
		id_cable = id_parts.index(agents['Cable']['id'])
		q1[id_cable,:] = q1[id_cable,:] - 200
		q = q1.flatten(order = 'F')

		# A matrix
		A1 = np.array([np.concatenate((np.full((i+1)*N_part,1),\
			np.full((N_time-1-i)*N_part,0))) for i in range(N_time)])
		A = sp.csc_matrix(A1)

		# l vector
		l = np.full(N_time,0)
		l[-1] = agents[name]['Efinal']-agents[name]['Enow']

		# u vector
		u = np.full(N_time, agents[name]['Emax']-agents[name]['Enow'])
	elif name == 'TES':
		# P matrix
		P = rho*sp.identity(N_time*N_part, format='csc')

		# q vector
		q1 = -rho*((trades[id_agent,id_parts,:]-trades[id_parts,id_agent,:])/2 \
			+ prices[id_agent,id_parts,:]/rho)
		# foster tes-nftl
		id_nftl = id_parts.index(agents['NFTL']['id'])
		q1[id_nftl,:] = q1[id_nftl,:] + 200
		q = q1.flatten(order = 'F')

		# A matrix
		A1 = np.array([np.concatenate((np.full((i+1)*N_part,1),\
			np.full((N_time-1-i)*N_part,0))) for i in range(N_time)])
		A = sp.csc_matrix(A1)

		# l vector
		l = np.full(N_time,-agents[name]['Enow'])
		# l[-1] = agents[name]['Efinal']-agents[name]['Enow']

		# u vector
		u = np.full(N_time, agents[name]['Emax']-agents[name]['Enow'])
	elif name == 'Cable':
		# P matrix
		P = rho*sp.identity(N_time*N_part, format='csc')

		# q vector
		q1 = -rho*((trades[id_agent,id_parts,:]-trades[id_parts,id_agent,:])/2 \
			+ prices[id_agent,id_parts,:]/rho)
		# encourage ev-cable
		id_ev = id_parts.index(agents['EV']['id'])
		q1[id_ev,:] = q1[id_ev,:] + 200
		q = q1.flatten(order = 'F')

		# A matrix
		a1 = np.full(N_part,1)
		A1 = sp.block_diag([a1 for i in range(N_time)],'csc')
		# limit line power
		id_ev = id_parts.index(agents['EV']['id'])
		id_pth = id_parts.index(agents['PTH']['id'])
		id_fh3 = id_parts.index(agents['Flex3']['id'])
		a2 = np.full(N_part,0)
		a2[id_ev] = 1
		a2[id_pth] = 1
		a2[id_fh3] = 1
		A2 = sp.block_diag([a2 for i in range(N_time)],'csc')
		A = sp.vstack([A1,A2], 'csc')
		# A = A1

		# l vector
		l = np.full(2*N_time, 0)
		l[N_time:] = -agents[name]['Pmax']
		# l = np.full(N_time, 0)

		# u vector 
		u = np.full(2*N_time, 0)
		u[N_time:] = -agents[name]['Pmin']
		# u = np.full(N_time, 0)

	elif name == 'PTH':
		# P matrix
		P = rho*sp.identity(N_time*N_part, format='csc')

		# q vector
		q1 = -rho*((trades[id_agent,id_parts,:]-trades[id_parts,id_agent,:])/2 \
			+ prices[id_agent,id_parts,:]/rho)
		# encourage ev-cable
		# id_ev = id_parts.index(agents['EV']['id'])
		# q1[id_ev,:] = q1[id_ev,:] + 200
		q = q1.flatten(order = 'F')

		# A matrix
		a1 = np.full(N_part,1)
		A = sp.block_diag([a1 for i in range(N_time)],'csc')

		# l vector
		l = np.full(N_time, 0)

		# u vector 
		u = np.full(N_time, 0)

	else:
		print('Error...')

	results = osqp_solve(P,q,A,l,u)
	new_trades[id_parts,:] = results.reshape((N_part,N_time), order = 'F')
	return new_trades

def perform_iteration(agents,trades,prices, rho):
	new_trades = trades.copy()
	for name,dic in agents.items():
		id_agent = dic['id']
		prices[id_agent,:,:] = price_update(name, agents, trades, prices, rho)
		new_trades[id_agent,:,:] = trades_update(name, agents, trades, prices, rho)
	trades[:,:,:] = new_trades


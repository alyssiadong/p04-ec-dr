'''
    slave_CEE.py
    Written by Ibrahim Al Asmi and Alyssia Dong for the 2020 DTU SYSLAB Course.

    Contains the slave part that corresponds to the simulated EV battery
    and the simulated thermal energy storage (TES).
'''

import pandas as pd
from syslab import SwitchBoard
import time
import logging
from xmlrpc.server import SimpleXMLRPCServer
import numpy as np
import threading
from syslab import Dumpload

# Battery model taken from dtu-mosaik
class MyBattSim:
    def __init__(
            self,
            rated_capacity=10,
            rated_discharge_capacity=20,
            rated_charge_capacity=20,
            roundtrip_efficiency=0.96,
            initial_charge_rel=0.50,
            charge_change_rate=0.9,
            dt=1.0/(60*60)):
        """
            Battery simulator.
            Time is assumed to proceed in integer steps.
            @input:
                rated_capacity: Max volume of charge [kWh]
                rated_discharge_capacity: Max output power [kW]
                rated_charge_capacity: Max input power [kW]
                roundtrip_efficiency: Total roundtrip efficiency [0.0-1.0]
                initial_charge_rel: Charge at time 0 relative to max [0.0 - 1.0]
                charge_change_rate: Rate at which charge follows setpoint,
                    1.0= instant following. [0.0 - 1.0]
                dt: number of hours per time step [h] (default: 1 sec per time step)
        """
        self.rated_capacity = rated_capacity
        self.rated_discharge_capacity = rated_discharge_capacity
        self.rated_charge_capacity = rated_charge_capacity
        self.eta = roundtrip_efficiency
        self.alpha = charge_change_rate

        # Internal variables
        self.curtime = 0 # Time is assumed to step in integer steps
        self.dt = dt
        self._P = 0
        self._Pset = 0
        self._charge = initial_charge_rel * rated_capacity

        # Externally visible variables
        self.Pext = 0
        self.Psetext = 0
        self.SoC = self._charge
        self.relSoC = self._charge / self.rated_capacity

    def calc_val(self, t):
        """
        """
        assert type(t) is int
        assert t >= self.curtime, "Must step to time of after or at current time: {0}. Was asked to step to {1}".format(t, self.curtime)
        self._ext_to_int() # Translate external state to internal state
        for _ in range(t - self.curtime):
            self._do_state_update()
        self._int_to_ext() # Translate internal state to external state
        self.curtime = t

    def _do_state_update(self):
        # Current power will slowly rise towards setpoint
        P = self.alpha * self._Pset + (1 - self.alpha) * self._P
        self._P = self._limit_P(P)
        self._update_charge()

    def _limit_P(self, P):
        # Limit according to bounds
        P = min(max(P, -self.rated_discharge_capacity), self.rated_charge_capacity)
        # Limit to available charge, roundtrip eff. is applied to output
        P = min(
                max(P, -self._charge * self.eta / self.dt), # Cannot discharge more than we have
                (self.rated_capacity - self._charge)/ self.dt) # Cannot overfill
        return P

    def _update_charge(self):
        C = self._charge + self._P / (self.eta if self._P>0 else 1) * self.dt
        self._charge = min(
                max(C, 0),
                self.rated_capacity)

    def _ext_to_int(self):
        self._Pset = - self.Psetext

    def _int_to_ext(self):
        self.Pext = - self._P
        self.SoC = self._charge
        self.relSoC = self._charge / self.rated_capacity

    # Getter/setters for external users
    @property
    def P(self):
        return self.Pext

    @property
    def Pset(self):
        return self.Psetext

    @Pset.setter
    def Pset(self, newPset):
        self.Psetext = min(self.rated_discharge_capacity,
                max(newPset, -self.rated_charge_capacity))

# Internal functions
def getTimestamp():
    t0 = pd.Timestamp.now()
    t1 = pd.Timestamp(year = 2018, month = 1, day = 25,\
        hour = t0.hour, minute = t0.minute, second = t0.second)
    return t1

def getSecond():
    t0 = pd.Timestamp.now()
    return t0.hour*3600 + t0.minute*60 + t0.second

# Initializing flags
TES_flag = 1
EV_flag = 1
CEE_flag = 1

name_1 = '716-2'
SB_connection = SwitchBoard(name_1)
print("connected to 716-2_CEE.".format(name_1))
measure_CEE = [0.] * 10
measure_EV = [0.] * 10
measure_TES = [0.] * 10
P_set_cee =[0.] *2

def getActivePower(syst):
    global measure_CEE
    global measure_EV
    global measure_TES

    mean_measure = 0

    if syst == "EV":
        mean_measure= EV.P
    elif syst == "TES":
        mean_measure = TES.P
    elif syst == "CEE":
        mean_measure = np.mean(measure_CEE)
    return float(mean_measure)

def thread_communication():
    x= threading.Thread(target=getActivePowerlocal)
    x.start()

# External functions
def getActivePowerlocal():
    global SB_connection
    global measure_CEE

    start = True

    while start == True :
        # Read the power measurement of PV or NFTL
        m_cee = SB_connection.getActivePower(0)
        measure_CEE.append(m_cee._value)
        measure_CEE = measure_CEE[1:]

        time.sleep(1)

def setActivePower(syst, Pset):
    global P_set_cee
    # Set the power of the storage elements
    logging.debug(f'Call received: setActivePower()')
    if syst == "EV":
        EV.calc_val(getSecond())
        EV.Pset = -Pset
        P_set_cee[0] = Pset
        # return 1
    elif syst == "TES":
        TES.calc_val(getSecond())
        TES.Pset = -Pset
        P_set_cee[1] = Pset
        # return 1
    else:
        return 0
    CEE = Dumpload('mobload3')
    CEE.setPowerSetPoint(sum(P_set_cee))
    logging.debug("Power Command CEE : {0}".format(sum(P_set_cee)))
    return 1

def test_syst(syst):
    if syst == "CEE":
        return CEE_flag
    elif syst == "EV" :
        return EV_flag
    elif syst == "TES":
        return TES_flag
    else:
        return 0

def getSoC(syst):
    # Read the current state of charge of the storage elements
    logging.debug(f'Call received: getSoC()')
    if syst == "EV":
        EV.calc_val(getSecond())
        return float(EV.SoC)
    elif syst == "TES":
        TES.calc_val(getSecond())
        return float(TES.SoC)
    else :
        return 0

EV = MyBattSim()
EV.calc_val(getSecond())
TES = MyBattSim(rated_capacity=30)
TES.calc_val(getSecond())
thread_communication()

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    server = SimpleXMLRPCServer(('localhost', 9002), logRequests=True)
    # Register the function we are serving
    server.register_function(getActivePower, "getActivePower")
    server.register_function(setActivePower, "setActivePower")
    server.register_function(getSoC, "getSoC")
    server.register_function(test_syst, "test_syst")
    try:
        print("Use Control-C to exit")
        # Start serving our functions
        server.serve_forever()
        
    except KeyboardInterrupt:
        print("Exiting")



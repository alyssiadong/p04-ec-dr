'''
    slave_716_2.py
    Written by Ibrahim Al Asmi and Alyssia Dong for the 2020 DTU SYSLAB Course.

    Contains the slave part that corresponds to the 716-2 building.
    A threaded function measures the power consumption/production 
    every second. 
    Returns the average power over the last 10s to the master.
'''

import pandas as pd
from syslab import SwitchBoard
import time
import logging
from xmlrpc.server import SimpleXMLRPCServer
import numpy as np
import threading


# Initializing flags
NFTL_flag = 1
Flex3_flag = 1
Cable_flag = 1

name = '716-2'
SB_connection = SwitchBoard(name)
print("connected to 716-2.".format(name))
measure_flexhouse3 = [0.] * 10
measure_heatpump = [0.] * 10
measure_heatbooster = [0.] * 10

# Internal functions
def getTimestamp():
    t0 = pd.Timestamp.now()
    t1 = pd.Timestamp(year = 2018, month = 1, day = 25,\
        hour = t0.hour, minute = t0.minute, second = t0.second)
    return t1

def getSecond():
    t0 = pd.Timestamp.now()
    return t0.hour*3600 + t0.minute*60 + t0.second

def getActivePower(syst):
    global measure_heatbooster
    global measure_flexhouse3
    global measure_heatpump

    mean_measure = 0

    if syst == "NFTL":
        mean_measure= np.mean(measure_heatpump)+np.mean(measure_heatbooster)
    elif syst == "Flex3":
        mean_measure = np.mean(measure_flexhouse3)

    return float(mean_measure)

def thread_communication():
    x= threading.Thread(target=getActivePowerlocal)
    x.start()

def getActivePowerlocal():
    global SB_connection
    global measure_heatpump
    global measure_flexhouse3
    global measure_heatbooster
    start = True

    while start == True :
        # Read the power measurement of PV or NFTL
        m_hp = SB_connection.getActivePower(6)
        measure_heatpump.append(m_hp._value)
        measure_heatpump = measure_heatpump[1:]

        m_fh3 = SB_connection.getActivePower(5)
        measure_flexhouse3.append(m_fh3._value)
        measure_flexhouse3 = measure_flexhouse3[1:]

        m_hb =SB_connection.getActivePower(7)
        measure_heatbooster.append(m_hb._value)
        measure_heatbooster = measure_heatbooster[1:]

        time.sleep(1)

# External functions
def test_syst(syst):

    if syst == "NFTL":
        return NFTL_flag
    elif syst == "Flex3":
        return Flex3_flag
    else:
        return 0

thread_communication()

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    server = SimpleXMLRPCServer(('localhost', 9000), logRequests=True)
    # Register the function we are serving
    server.register_function(getActivePower, "getActivePower")
    server.register_function(test_syst, "test_syst")
    try:
        print("Use Control-C to exit")
        # Start serving our functions
        server.serve_forever()
        
    except KeyboardInterrupt:
        print("Exiting")



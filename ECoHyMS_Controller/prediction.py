'''
    prediction.py
    Written by Ibrahim Al Asmi and Alyssia Dong for the 2020 DTU SYSLAB Course.

    Contains the functions used to predict non flexible consumption 
    and production.

'''

#import packages
import numpy as np
import math
from sklearn.metrics import mean_squared_error
import pandas as pd

# This simple function is responsable for  predicting wind turbine production and flexhouses consumption with a AR1 model (mean and equivelant noise around). 
# The inputs are typical days for power datas, start time for the prediction and time step of the prediction function
# The output of this function is a vector of n size correponding to the prediction from start time till the end of the day

def g_noise_pred(t_start, delta_t, series_loc="dtu_mosaik/signals.h5",series_titles=['/flexhouse_20180220','/flexhouse_20180221','/flexhouse_20180219'], mean_real=None):
#    '/flexhouse_20180219','/flexhouse_20180219','/flexhouse_20180219','/flexhouse_20180219'

    full_array=launch_prediction_pv(series_loc,series_titles)
    a=np.array([0]*full_array.shape[1])
    if len(full_array.shape) > 1 :
        for i in range(len(full_array)):
            a=a+full_array[i]
            a=a/full_array.shape[0]
        full_array = a

    array_size = len(full_array)
    prediction_size = int(round(array_size/delta_t))
    p_pred_v = [0]* prediction_size
    train = np.array([np.nan_to_num(np.mean(full_array[i:i + delta_t])) for i in range(array_size) if i % delta_t == 0])
    
    #calculate the mean of the day power data
    mean = np.mean(train)
    mean_v = [mean] * prediction_size
    
    #calcutate the mean squared error in order to estimate a sigma for a AR1 model
    error = mean_squared_error(train, mean_v)
    if mean_real != None:
        mean = mean_real
    sigma = math.sqrt(error)
    for i in range(prediction_size):
        while p_pred_v[i] < 1e-3:
            p_pred_v[i] =  mean + (np.random.randn()) * sigma
    return p_pred_v[int(t_start/delta_t):]
    
# This function is very similar to the one above except that it is adapted for PV typical days data

def g_noise_pred_pv(t_start, delta_t,series_loc="dtu_mosaik/signals.h5",series_titles=['PV715_20180125','PV715_20180126','PV715_20180127','PV715_20180130']):
    ### 'PV715_20180125','PV715_20180126','PV715_20180127','PV715_20180130'
    full_array=launch_prediction_pv(series_loc,series_titles)
    a=np.array([0]*full_array.shape[1])
    full_array = - full_array
    if len(full_array.shape) > 1 :
        for i in range(len(full_array)):
            a=a+full_array[i]
            a=a/full_array.shape[0]
        full_array = a

    array_size = len(full_array)
    prediction_size = int(round(array_size/delta_t))
    p_pred_v = [-0.5]* prediction_size

    train = -np.array([np.mean(full_array[i:i + delta_t]) for i in range(array_size) if i % delta_t == 0])
    
    #algorithm to detect the time corresponding to sunrise and sunset
    u=0
    while 0.01 > train[u] :
        u = u+1
    i_st = u
    while 0.01 < train[u] :
        u = u+1
    i_ed = u

    train = train[i_st:i_ed]
    x=np.arange(i_ed-i_st)
    params = np.polyfit(x,train,3)
    mean_v =  params[0]*x**3 + params[1] *x**2 + params[2]*x +params[3]
    mean_v[mean_v<0.] = 0
    error = mean_squared_error(train, mean_v)
    sigma = math.sqrt(error)

    for i in range(prediction_size):
        if i < i_st :
            p_pred_v[i] = 0.
        elif  i >= i_ed :
            p_pred_v[i] = 0.
        else :
            while p_pred_v[i]< min(mean_v):
                p_pred_v[i] =  mean_v[i-i_st] + (np.random.randn()) * sigma
    p_pred_v = [-x for x in p_pred_v]
    return p_pred_v[int(t_start/delta_t):]

# In case where typical days are multiple (more than one), this function average the different days for a one vector base prediction
def launch_prediction_pv(series_loc, series_titles):
    store = pd.HDFStore(series_loc)
    x = store[series_titles[0]]
    pred_array = np.nan_to_num(np.array([x]))
    max_size= max(pred_array.shape)
    max_size = max_size-100
    pred_array = np.array([x[:max_size]])

    if len(series_titles)>1:
        for i in range(len(series_titles)-1):
            x = store[series_titles[i+1]]
            x = np.nan_to_num(np.array([x[:max_size]]))
            pred_array=np.append(pred_array[:max_size], x,0)
    return pred_array
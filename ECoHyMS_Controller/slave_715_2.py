'''
    slave_715_2.py
    Written by Ibrahim Al Asmi and Alyssia Dong for the 2020 DTU SYSLAB Course.

    Contains the slave part that corresponds to the 715-2 building.
    A threaded function measures the power consumption/production 
    every second. 
    Returns the average power over the last 10s to the master.
'''

import pandas as pd
from syslab import SwitchBoard
import time
import logging
from xmlrpc.server import SimpleXMLRPCServer
import numpy as np
import threading


# Initializing flags
PV_flag = 1
Gaia_flag = 1
Flex1_flag = 1
EV_flag = 1
Cable_flag = 1

name = '715-2'
SB_connection = SwitchBoard(name)
print("connected to 715-2.".format(name))
measure_flexhouse1 = [0.] * 10
measure_gaia = [0.] * 10
measure_pv = [0.] * 10
measure_grid = [0.] * 10

def getActivePower(syst):
    global measure_pv
    global measure_flexhouse1
    global measure_grid
    global measure_gaia

    mean_measure = 0

    if syst == "Gaia":
        mean_measure= np.mean(measure_gaia)
    elif syst == "PCC":
        mean_measure = np.mean(measure_grid)
    elif syst == "PV":
        mean_measure = np.mean(measure_pv)
    elif syst == "Flex1":
        mean_measure = np.mean(measure_flexhouse1)
    return float(mean_measure)

def thread_communication():
    x= threading.Thread(target=getActivePowerlocal)
    x.start()

def getActivePowerlocal():
    global SB_connection
    global measure_pv
    global measure_flexhouse1
    global measure_grid
    global measure_gaia

    start = True

    while start == True :
        # Read the power measurement of PV or NFTL
        m_pv = SB_connection.getActivePower(3)
        measure_pv.append(m_pv._value)
        measure_pv = measure_pv[1:]

        m_fh1 = SB_connection.getActivePower(2)
        measure_flexhouse1.append(m_fh1._value)
        measure_flexhouse1 = measure_flexhouse1[1:]

        m_grid =SB_connection.getActivePower(0)
        measure_grid.append(m_grid._value)
        measure_grid = measure_grid[1:]

        m_gaia =SB_connection.getActivePower(1)
        measure_gaia.append(m_gaia._value)
        measure_gaia = measure_gaia[1:]

        time.sleep(1)

# External functions
def test_syst(syst):
    if syst == "PV":
        return PV_flag
    elif syst == "Gaia":
        return Gaia_flag
    elif syst == "Heatpump":
        return Heatpump_flag
    elif syst == "Flex1":
        return Flex1_flag
    elif syst == "Flex3":
        return Flex3_flag
    elif syst == "EV" :
        return EV_flag
    elif syst == "Cable":
        return Cable_flag
    else:
        return 0

thread_communication()

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    server = SimpleXMLRPCServer(('localhost', 9001), logRequests=True)
    # Register the function we are serving
    server.register_function(getActivePower, "getActivePower")
    server.register_function(test_syst, "test_syst")
    try:
        print("Use Control-C to exit")
        # Start serving our functions
        server.serve_forever()
        
    except KeyboardInterrupt:
        print("Exiting")



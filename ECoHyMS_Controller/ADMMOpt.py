'''
    ADMMOpt.py
    Written by Ibrahim Al Asmi and Alyssia Dong for the 2020 DTU SYSLAB Course.

    Contains the class that manages the ADMM variables, their updates and   
    communicates with the slaves (read and set power). 

'''

import sys
import socket
import asyncio
import nest_asyncio
from statistics import mean
import time
import pandas as pd
import numpy as np
from util import perform_iteration
from prediction import g_noise_pred_pv as gnpv
from prediction import g_noise_pred as gnp
from aioxmlrpc.client import ServerProxy

nest_asyncio.apply()    # Needed in order to work with spyder 

def getSecond():
    t0 = pd.Timestamp.now()
    return t0.hour*3600 + t0.minute*60 + t0.second

class ADMMOpt:
    # All active power and energy values are in kW and kWh
    def __init__(self, Ntime):
        self.rho = 20
        self.k = 0
        self.kmax = 300
        self.agents = {
            'PCC':{
                'id':0,
                'partners':['PV','Gaia','Flex1', 'Cable'],
                'Pmin':-100,
                'Pmax':100,
                'Pnow':0,
                'Ppred':0,
                'address':'http://localhost:9001',
                # 'address':'http://10.42.245.121:9001',
            },
            'PV':{
                'id':1,
                'partners':['PCC','Flex1', 'Cable'],
                'Pmin':-34.5,
                'Pmax':0,
                'Pnow':-8,
                'Ppred':0,
                'address':'http://localhost:9001',
                # 'address':'http://10.42.245.121:9001',
            },
            'Cable':{
                'id':2,
                'partners':['PCC','Gaia', 'PV', 'Flex3', 'PTH', 'EV'],
                'Pmin':0,
                'Pmax':10,####
                'address':'',
            },
            'EV':{
                'id':3,
                'partners':['Cable'],
                'Pmin':0,
                'Pmax':43.47,
                'Pnow':3,
                'Emax':8,
                'Enow':0,
                'address':'http://localhost:9002',
                # 'address':'http://10.42.245.121:9002',
            },
            'PTH':{
                'id':4,
                'partners':['Cable', 'NFTL', 'TES'],
                'Pmin':0,
                'Pmax':40,
                'address':'',
            },
            'Flex1':{
                'id':5,
                'partners':['PCC','PV', 'Gaia'],
                'Pmin':0,
                'Pmax':11,
                'address':'http://localhost:9001',
                # 'address':'http://10.42.245.121:9001',
            },
            'Flex3':{
                'id':6,
                'partners':['Cable'],
                'Pmin':0,
                'Pmax':6,
                'address':'http://localhost:9000',
                # 'address':'http://10.42.245.121:9000',
            },
            'Gaia':{
                'id':7,
                'partners':['PCC','Flex1', 'Cable'],
                'Pmin':0,
                'Pmax':3,
                'address':'http://localhost:9001',
                # 'address':'http://10.42.245.121:9001',
            },
            'TES':{
                'id':8,
                'partners':['PTH', 'NFTL'],
                'Pmin':-10,
                'Pmax':10,
                'Emax':30,
                'Enow':0,
                'address':'http://localhost:9002',
                # 'address':'http://10.42.245.121:9002',
            },
            'NFTL':{
                'id':9,
                'partners':['PTH', 'TES'],
                'Pmin':0,
                'Pmax':17,
                'address':'http://localhost:9000',
                # 'address':'http://10.42.245.121:9000',
            },
        }

        self.Ntime = Ntime
        self.Nagents = len(self.agents)
        self.agents['EV']['Efinal'] = self.agents['EV']['Emax']*0.9
        self.agents['TES']['Efinal'] = self.agents['TES']['Emax']*0.5

        self._trades = np.zeros((self.Nagents,self.Nagents,Ntime))
        self._prices = np.zeros((self.Nagents,self.Nagents,Ntime))

        self._trades_solution = np.empty_like(self._trades)
        self._prices_solution = np.empty_like(self._prices)
        self._power_solution = np.zeros(self.Nagents)

        self._primal_residual = 0

        self._status = "Initialized"

    async def read_value(self, syst):
        # Read non flexible consumption or production of 'syst'
        # via rpc communication through the corresponding slave.

        async with ServerProxy(self.agents[syst]['address']) as proxy:
            P = await proxy.getActivePower(syst)
            if syst in ['Gaia', 'PV']:
                if P>0:
                    P = -P
            if syst in ['Heatpump', 'Flex1', 'Flex3', 'NFTL']:
                if P<0:
                    P = -P
                if (P > self.agents[syst]["Pmax"]) or (P < self.agents[syst]["Pmin"]):
                    print(("Warning : the measured power of NFL ", syst, " is out of bounds. P_read = ", P))
            self.agents[syst]["Pnow"] = P
            return self.agents[syst]["Pnow"]

    async def read_SoC(self, syst):
        # Read state of charge of 'syst'
        # via rpc communication through the corresponding slave.

        async with ServerProxy(self.agents[syst]['address']) as proxy:
            E = await proxy.getSoC(syst)
            if E<0:
                self.agents[syst]["Enow"] = -E
            else:
                self.agents[syst]["Enow"] = E
            return self.agents[syst]["Enow"]

    def set_prediction(self, syst = None, values = None):
        # Set the prediction for the optimization.
        # Three types of prediction: 
        #   - dumb prediction: the non flexible consumption/production
        #   constant during the rest of the day
        #   - smart prediction: using 'prediction.py'
        #   - pessimistic prediction: (for production) there won't 
        #   be any production for the rest of the day

        if (syst == None) and (values == None):
            # Dumb prediction for all systems
            self.agents['PV']['Ppred'] = np.full(self.Ntime,self.agents['PV']['Pnow'])
            print(("PV", self.agents['PV']['Ppred']))
            self.agents['Gaia']['Ppred'] = np.full(self.Ntime,self.agents['Gaia']['Pnow'])
            print(("Gaia", self.agents['Gaia']['Ppred']))
            self.agents['NFTL']['Ppred'] = np.full(self.Ntime,self.agents['NFTL']['Pnow'])
            print(("NFTL", self.agents['NFTL']['Ppred']))
            self.agents['Flex1']['Ppred'] = np.full(self.Ntime,self.agents['Flex1']['Pnow'])
            print(("Flex1", self.agents['Flex1']['Ppred']))
            self.agents['Flex3']['Ppred'] = np.full(self.Ntime,self.agents['Flex3']['Pnow'])
            print(("Flex3", self.agents['Flex3']['Ppred']))
            self._status = "Ready"
        elif (syst != None) and (values == None):
            # Dumb prediction
            self.agents[syst]['Ppred'] = np.full(self.Ntime,self.agents[syst]['Pnow'])
            print((syst, self.agents[syst]['Ppred']))

        elif (syst == None) or (values == None):
            raise Exception('syst must be defined if values is defined')
        else:
            assert len(values) == self.Ntime
            # if (syst == "Gaia"):
            #     self.agents[syst]['Ppred'] = np.full_like(values, self.agents[syst]['Pnow'])
            #     self._status = "Ready"
            # else:
            if syst == 'Gaia':
                # Make the system believe there will be no wind in the future
                self.agents[syst]['Ppred'] = np.full_like(values, 0)
                self.agents[syst]['Ppred'][0] = self.agents[syst]['Pnow']
            else:
                self.agents[syst]['Ppred'] = np.array(values)
                self.agents[syst]['Ppred'][0] = self.agents[syst]['Pnow']
            self._status = "Ready"
            print((syst, self.agents[syst]['Ppred']))

    async def set_power_storage(self, syst, val=None):
        # Set storage power setpoint
        # via rpc communication through the corresponding slave
        if val == None :
            val = self._power_solution[self.agents[syst]["id"],0]
        async with ServerProxy(self.agents[syst]['address']) as proxy:
            res = await proxy.setActivePower(syst, float(val))
        return res

    def warm_start(self, trades0, prices0):
        # Unused
        self._trades = trades0
        self._prices = prices0

        self._trades_solution = np.empty_like(self._trades)
        self._prices_solution = np.empty_like(self._prices)
        self._power_solution = np.zeros(self.Nagents)

        self._primal_residual = 0

        self._status = "Initialized"
        self.k = 0

    def compute_primal_res(self):
        # The convergence of the ADMM optimization is defined by 
        # its primal residual.
        self._primal_residual= np.sum((self._trades[:,:,0] +\
            np.transpose(self._trades[:,:,0]))**2)
        return self._primal_residual

    def iteration(self):
        # ADMM optimization iteration
        if self._status == "Ready":
            print("Constraints are not set yet...")
            return 0
        if (self.k<self.kmax) and (self._status == "Processing"):
            perform_iteration(self.agents, self._trades, self._prices, self.rho)
            self.k += 1
            if (self.compute_primal_res() < 0.05) and (self.k>8):
                self._status = "Done"
        elif self.k>=self.kmax:
            self._status = "Iteration limit"

        if self._status == "Done":
            self._trades_solution = self._trades
            self._prices_solution = self._prices
            self._power_solution = np.sum(self._trades, axis =1)
            return 1
        else:
            return 0

    def launch_resolution(self):
        # Global ADMM optimization
        if self._status == "Ready":
            self._status = "Processing"
        res = 0
        while (res == 0) and (self._status == "Processing"):
            res = self.iteration()
            print(("iteration", self.k, "primal res", self._primal_residual))
        if self._status == "Done":
            print("Resolution done. ")
            # self.set_power_storage()
        elif self._status == "Iteration limit":
            print("Iteration limit reached.")

    @property
    def _power(self):
        return np.sum(self._trades, axis = 1)
    
    @property
    def power(self):
        return self._power_solution

    @property
    def trades(self):
        return self._trades_solution

    @property
    def prices(self):
        return self._prices_solution

    @property
    def status(self):
        return self._status

    @property 
    def power_res(self):
        power = self._power_solution[:,0]
        return [(key, power[self.agents[key]["id"]]) for key in self.agents.keys()]


'''
    StateMachine.py
    Written by Ibrahim Al Asmi and Alyssia Dong for the 2020 DTU SYSLAB Course.

    Contains the state machine of the SYSLAB-ECoHyMS controller.
    Allows to do error management and make the controller running.
    Save results at each optimization step properly finished.
'''

from transitions import Machine
import time
import sys
import asyncio
import nest_asyncio
from syslab import SwitchBoard
import threading
from statistics import mean
import pandas as pd
import numpy as np
from prediction import g_noise_pred_pv as gnpv
from prediction import g_noise_pred as gnp
from ADMMOpt import ADMMOpt
from aioxmlrpc.client import ServerProxy

nest_asyncio.apply()    # Needed in order to work with spyder

async def init_syst(syst):
    if syst in ['PV', 'PCC', 'Flex1', 'Gaia']:
        # address = 'http://10.42.245.121:9001'
        address = 'http://localhost:9001'
    elif syst in ['TES', 'EV']:
        # address = 'http://10.42.245.121:9002'
        address = 'http://localhost:9002'
    else:
        # address = 'http://10.42.245.121:9000'
        address = 'http://localhost:9000'
    async with ServerProxy(address) as proxy:
        res = await proxy.test_syst(syst)
    return res

def getSecond():
    t0 = pd.Timestamp.now()
    return t0.hour*3600 + t0.minute*60 + t0.second


'''
    SyslabController class
'''
class SyslabController(object):

    states = [
        'Init0',
        'Initialization', 
        'SetConstraints', 
        'Optimization', 
        'SetPower', 
        'Done']
    K1lim = 5   
    K2lim = 60
    # K2lim = 5

    def __init__(self, setPower = True):

        # Initialize the state machine
        self.machine = Machine(model = self, 
            states = SyslabController.states, initial = 'Init0')
        self.k1 = 0
        self.k2 = 0
        self.T0 = getSecond()
        self._set_power = setPower
        self.dt = 5

        # Restart transition when calling for self.restart()
        self.machine.add_transition(
            trigger = 'restart', 
            source = 'Done' , 
            dest = 'Initialization')

        self._P_PV = 0
        self._P_Gaia = 0
        self._P_EV = 0
        self._P_TES = 0
        self._P_NFTL = 0
        self._P_Flex1 = 0
        self._P_Flex3 = 0
        self._SoC_EV = 0
        self._SoC_TES = 0
        self._P_Grid = 0

        t0 = pd.Timestamp.now()
        timename = t0.strftime('%Y-%m-%d_%H-%M-%S')
        self.filename = 'results/' + timename + '.csv'

        self.df = pd.DataFrame( [[ timename,
                                self._P_PV,
                                self._P_Gaia,
                                self._P_EV,
                                self._P_TES,
                                self._P_NFTL,
                                self._P_Flex1,
                                self._P_Flex3,
                                self._SoC_EV,
                                self._SoC_TES,
                                self._P_Grid]],
            columns = [ 'datetime','P_PV', 'P_Gaia', 'P_EV', 
            'P_TES', 'P_NFTL', 'P_Flex1','P_Flex3', 'SoC_EV', 'SoC_TES', 'P_Grid'])

        self.to_Initialization()

    '''
    Add state callbacks
    '''
    def on_enter_Initialization(self):
        print("Starting initialization...")
        self.k1 = 0
        self.k2 = 0

        self.res_list = []

        pending = asyncio.run(self.async_check())

        if len(pending) == 0:
            self.to_SetConstraints()
        else:
            print("Initialization failed.")
            self.waitfor10s_init()
            self.to_Initialization()

    def on_exit_Initialization(self):
        print("Initialization done")

    def on_enter_SetConstraints(self):
        print("Performing prediction and reading instant power values")
            
        self.prediction()
        self.opt_set()
        self.T0 = getSecond()
        asyncio.run(self.async_read())
        self.set_prediction()
        self.to_Optimization()

    def on_enter_Optimization(self):
        print(("Optimization step", self.k2))
        self.opt.launch_resolution()

        if (self.opt.status == "Done") :
            self.to_SetPower()
        elif (self.opt.status == "Iteration limit") and self.check_k1():
            self.increment_k1()
            asyncio.run(self.opt.set_power_storage("EV",0))
            asyncio.run(self.opt.set_power_storage("TES",0))
            self.to_SetConstraints()
        else:
            print("Optimization error : setting power set points to zero")
            asyncio.run(self.opt.set_power_storage("EV",0))
            asyncio.run(self.opt.set_power_storage("TES",0))
            self.waitfor10s_init()
            self.to_Initialization()

    def on_enter_SetPower(self):
        print("Setting power of flexible units")
        self.store_res()
        res = asyncio.run(self.set_power_points())
        T = getSecond()
        time_to_wait = self.dt-(T-self.T0) if (T-self.T0 < self.dt) else 1
        if res and self.check_k2() :
            self.to_Done()
        elif res:
            self.increment_k2()
            self.waitfor(time_to_wait)
            self.to_SetConstraints()
        else:
            print("Warning : Could not set flexible unit power point. Retrying...")
            self.waitfor2s()
            self.to_SetPower()

    def on_enter_Done(self):
        print("Test is done !")

    '''
    Functions definition
    '''
    def waitfor2s(self):
        time.sleep(2)

    def waitfor10s_init(self):
        print("Waiting 10s before initialization...")
        time.sleep(10)

    def waitfor(self, value):
        time.sleep(value)

    def increment_k1(self):
        self.k1 += 1

    def check_k1(self):
        # Restart optimization if it didn't converge
        # after 5 tries
        return self.k1 < SyslabController.K1lim

    def increment_k2(self):
        self.k2 += 1

    def check_k2(self):
        return self.k2 >= SyslabController.K2lim

    '''
    Asynchronous check that all units respond
    Called during 'Initialization' state
    '''
    async def async_check(self):
        task_PV = asyncio.create_task(init_syst("PV"))
        task_Gaia = asyncio.create_task(init_syst("Gaia"))
        task_EV = asyncio.create_task(init_syst("EV"))
        task_TES = asyncio.create_task(init_syst("TES"))
        task_NFTL = asyncio.create_task(init_syst("NFTL"))
        task_Flex1 = asyncio.create_task(init_syst("Flex1"))
        task_Flex3 = asyncio.create_task(init_syst("Flex3"))
        task_Cable = asyncio.create_task(init_syst("Cable"))

        done,pending = await asyncio.wait(
            [
            task_PV,
            task_Gaia,
            task_EV,
            task_TES,
            task_NFTL,
            task_Flex1,
            task_Flex3,
            task_Cable
                ],
            timeout = 10
        )

        return pending

    '''
    Prediction
    '''
    def prediction(self):
        self.T1 = getSecond()
        if self.k2 == 0:
            # self.PV_pred = gnpv(self.T1, 60*60, series_loc="dtu_mosaik/signals.h5",series_titles=['PV715_20180130'])
            self.PV_pred = gnpv(self.T1, 60*60)
            self.Gaia_pred = (-np.array(gnp(self.T1, 60*60, "dtu_mosaik/signals.h5",['/Gaia_20180511',]))).tolist()
            self.NFTL_pred = gnp(self.T1, 60*60, mean_real = 5)
            self.Flex1_pred = gnp(self.T1, 60*60)
            self.Flex3_pred = gnp(self.T1, 60*60)
        self.N_hours = len(self.PV_pred)

    '''
    Optimization object instanciation
    '''
    def opt_set(self):
        self.opt = ADMMOpt(self.N_hours)


    '''
    Asynchronous reading of power and setting of prediction 
    If one of the power/SoC reading has timed out, the previous value is used
    '''
    async def async_read(self):
        task_PV = asyncio.create_task(self.opt.read_value("PV"))
        task_Gaia = asyncio.create_task(self.opt.read_value("Gaia"))
        task_EV = asyncio.create_task(self.opt.read_SoC("EV"))
        task_TES = asyncio.create_task(self.opt.read_SoC("TES"))
        task_NFTL = asyncio.create_task(self.opt.read_value("NFTL"))
        task_Flex1 = asyncio.create_task(self.opt.read_value("Flex1"))
        task_Flex3 = asyncio.create_task(self.opt.read_value("Flex3"))

        tasks = [task_PV,task_Gaia,task_EV,task_TES,
            task_NFTL,task_Flex1,task_Flex3]

        done,pending = await asyncio.wait(tasks, timeout = 10)

        if task_PV in done:
            self._P_PV = task_PV.result()
            print(("_P_PV",self._P_PV))
        else:
            self.opt.agents["PV"]["Pnow"] = self._P_PV
        if task_Gaia in done: 
            self._P_Gaia = task_Gaia.result()
            print(("_P_Gaia",self._P_Gaia))
        else:
            self.opt.agents["Gaia"]["Pnow"] = self._P_Gaia
        if task_EV in done: 
            self._SoC_EV = task_EV.result()
            print(("_SoC_EV",self._SoC_EV))
        else:
            self.opt.agents["EV"]["Enow"] = self._SoC_EV
        if task_TES in done: 
            self._SoC_TES = task_TES.result()
            print(("_SoC_TES",self._SoC_TES))
        else:
            self.opt.agents["TES"]["Enow"] = self._SoC_TES
        if task_NFTL in done: 
            self._P_NFTL = task_NFTL.result()
            print(("_P_NFTL",self._P_NFTL))
        else:
            self.opt.agents["NFTL"]["Pnow"] = self._P_NFTL
        if task_Flex1 in done: 
            self._P_Flex1 = task_Flex1.result()
            print(("_P_Flex1",self._P_Flex1))
        else:
            self.opt.agents["Flex1"]["Pnow"] = self._P_Flex1
        if task_Flex3 in done: 
            self._P_Flex3 = task_Flex3.result()
            print(("_P_Flex3",self._P_Flex3))
        else:
            self.opt.agents["Flex3"]["Pnow"] = self._P_Flex3


        # print(done)
        if len(pending) != 0:
            print("Warning : not all constraints were updated.")

    def set_prediction(self):
        self.opt.set_prediction("PV", self.PV_pred)
        self.opt.set_prediction("Gaia", self.Gaia_pred)
        self.opt.set_prediction("NFTL", self.NFTL_pred)
        # self.opt.set_prediction("Flex1", self.Flex1_pred)
        self.opt.set_prediction("Flex1")
        # self.opt.set_prediction("Flex3", self.Flex3_pred)
        self.opt.set_prediction("Flex3")
        # self.opt.set_prediction()

    '''
    Asynchronous setting power points of flexible units 
    '''
    async def set_power_points(self):
        self._P_EV = self.opt._power_solution[self.opt.agents["EV"]["id"],0]
        self._P_TES = self.opt._power_solution[self.opt.agents["TES"]["id"],0]

        self._P_Grid = -(self._P_PV + \
                    self._P_Gaia + \
                    self._P_TES + \
                    self._P_NFTL + \
                    self._P_EV + \
                    self._P_Flex1 + \
                    self._P_Flex3
                    )

        if self._set_power:
            task_EV = asyncio.create_task(self.opt.set_power_storage("EV"))
            task_TES = asyncio.create_task(self.opt.set_power_storage("TES"))

            tasks = [task_EV, task_TES]
            done,pending = await asyncio.wait(tasks, timeout = 10)
            return (len(pending) ==0)
        else:
            print("Warning : self._set_power is set to False. The results are not implemented in Syslab.")
            return True
    '''
    Store and plot results 
    '''
    def store_res(self):
        # Store results of every optimization iteration done during the test
        print("Save results")
        t0 = pd.Timestamp.now()
        timename = t0.strftime('%Y-%m-%d_%H-%M-%S')

        df2 = pd.DataFrame( [[ timename,
                                self._P_PV,
                                self._P_Gaia,
                                self._P_EV,
                                self._P_TES,
                                self._P_NFTL,
                                self._P_Flex1,
                                self._P_Flex3,
                                self._SoC_EV,
                                self._SoC_TES,
                                self._P_Grid]],
            columns = [ 'datetime','P_PV', 'P_Gaia', 'P_EV', 
            'P_TES', 'P_NFTL', 'P_Flex1','P_Flex3', 'SoC_EV', 'SoC_TES', 'P_Grid'])
        self.df = self.df.append(df2, ignore_index=True)

        self.df.to_csv(self.filename) 

    def plot_res(self):
        self.df.plot(x='datetime', y = ['P_PV', 'P_Gaia', 'P_EV', 
            'P_TES', 'P_NFTL', 'P_Flex1', 'P_Flex3', 'P_Grid'])     


# if __name__ == "__main__":
#     stop_threads = False
#     t = threading.Thread(target\
#         =monitor_syslab,args=())
#     t.start()
#     # sm = SyslabController(setPower = True)

#     try:
#         sm = SyslabController(setPower = True)
#     except:
#         print('Interruption')
#     stop_threads = True
#     t.join() 
#     print('thread killed') 
 
    